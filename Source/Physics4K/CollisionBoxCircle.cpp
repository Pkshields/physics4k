////////////////////////////////////////////////////////////////////////////////
// Filename: CollisionBoxCircle.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Collision.h"

#pragma region PREDEFINITION

Vector2f ClosestPointToOBB(Box* box, Vector2f& point);

#pragma endregion

/**
 * Calculate the collision data between two circles
 *
 * @box				OBB Box to try and collide with...
 * @circle			... A circle, which is this
 * @contacts		Contacts array for us to store our contacts in
 * @isCircleFirst	For the collision resolution, the normal of the contact needs to be in the direction of the first shape	
 *					in the BoxPair. This will determine which direction the normal is facing
 */
int PK::BoxCircleCollision(Box* box, Circle* circle, Contact* contacts, bool isCircleFirst)
{
	//Calculate the closest point on the box to the circle
	Vector2f closestOBBPoint = ClosestPointToOBB(box, circle->Position);
	
	//Calculate the (squared) distance from the point above to the center of the circle
	Vector2f distanceVector = closestOBBPoint - circle->Position;
	float distanceSquared = Dot(distanceVector, distanceVector);

	//If the (squared) distance is smaller than the (squared) radius of the circle, then we have a collision!
	if (distanceSquared <= circle->Radius * circle->Radius)
	{
		//For this, we are doing what we did for Circle Circle and kinda just making up the contact points.
		//We are using the closest point to the circle center for our contact point
		distanceVector.Normalize();

		contacts[0].Separation = (FastSqrt((circle->Radius * circle->Radius) - distanceSquared) * -1.f);// + 0.5f;
		contacts[0].Normal = distanceVector * (isCircleFirst ? 1 : -1);
		contacts[0].Position = closestOBBPoint;

		contacts[1].Separation = contacts[0].Separation;
		contacts[1].Normal = contacts[0].Normal;
		contacts[1].Position = contacts[0].Position;

		return 2;
	}

	//Else, no collision
	return 0;
}


/**
 * Calculate the closest point on an OBB to a circle
 *
 * @box				OBB Box to calculate the pount from
 * @point			Point to find the closest point to
 */
Vector2f ClosestPointToOBB(Box* box, Vector2f& point)
{
	//Translate the point into local OBB space
	Vector2f distance = point - box->Position;

	//Rotate the distance point into the local space of the OBB
	Matrix22 rotMatrix2 = Matrix22(box->Rotation);
	rotMatrix2.Transpose();							//As we are now working in local space, a transpose is needed to rotate in the direction we want
	distance = rotMatrix2 * distance;

	//Set the resulting vector to th box position to that translating back is easy.
	Vector2f result = box->Position;

	//Calculate the two box axis' using the rotation matrix
	Matrix22 rotMatrix = Matrix22(0.f);
	Vector2f widthAxis = rotMatrix.Column1;
	Vector2f heightAxis = rotMatrix.Column2;

	//Calculate the halfWidth
	Vector2f halfWidth = box->GetDimensions() * 0.5f;

	//Get the distance in the X axis, corresponding to the width
	float widthDistance = distance.X;

	//If distance farther than the box extents, clamp to the box
	if (widthDistance > halfWidth.X) widthDistance = halfWidth.X;
	if (widthDistance < -halfWidth.X) widthDistance = -halfWidth.X;

	//Get the distance in the X axis, corresponding to the height
	float heightDistance = distance.Y;

	//If distance farther than the box extents, clamp to the box
	if (heightDistance > halfWidth.Y) heightDistance = halfWidth.Y;
	if (heightDistance < -halfWidth.Y) heightDistance = -halfWidth.Y;

	//Rotate the point back into the world rotation
	rotMatrix2.Transpose();
	distance = rotMatrix2 * Vector2f(widthDistance, heightDistance);

	//Compile the final world positioned point, return it
	result.X += distance.X;
	result.Y += distance.Y;

	////Translate the point into local OBB space
	//Vector2f distance = point - box->Position;

	////Set the resulting vector to th box position to that translating back is easy.
	//Vector2f result = box->Position;

	////Calculate the two box axis' using the rotation matrix
	//Matrix22 rotMatrix = Matrix22(box->Rotation);
	//Vector2f widthAxis = rotMatrix.Column1;
	//Vector2f heightAxis = rotMatrix.Column2;

	////Calculate the haldWidth
	//Vector2f halfWidth = box->GetDimensions() * 0.5f;

	////Map the distance position onto the width axis
	//float widthDistance = Dot(distance, widthAxis);

	////If distance farther than the box extents, clamp to the box
	//if (widthDistance > halfWidth.X) widthDistance = halfWidth.X;
	//if (widthDistance < -halfWidth.X) widthDistance = -halfWidth.X;

	////Map the distance position onto the height axis
	//float heightDistance = Dot(distance, heightAxis);

	////If distance farther than the box extents, clamp to the box
	//if (heightDistance > halfWidth.Y) heightDistance = halfWidth.Y;
	//if (heightDistance < -halfWidth.Y) heightDistance = -halfWidth.Y;

	////Compile the final world positioned point, return it
	//result.X += widthDistance;
	//result.Y += heightDistance;

	return result;
}
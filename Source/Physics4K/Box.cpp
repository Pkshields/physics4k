////////////////////////////////////////////////////////////////////////////////
// Filename: Box.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Box.h"

using namespace PK;

/**
 * Create an instance of a Box object
 *
 * @xPos		Position of the box in the X axis
 * @yPos		Position of the box in the Y axis
 * @width		Width of the box
 * @height		Height of the box
 * @rotation	Rotation of the box in radians
 * @mass		Mass of the box
 */
Box::Box(float xPos, float yPos, float width, float height, float rotation, float mass)
{
	Initialize(xPos, yPos, width, height, rotation, mass);

	Type = ShapeType::BoxType;
}

/**
 * Create an instance of a Box object
 *
 * @xPos		Position of the box in the X axis
 * @yPos		Position of the box in the Y axis
 * @width		Width of the box
 * @height		Height of the box
 * @rotation	Rotation of the box in radians
 * @mass		Mass of the box
 * @friction	Coefficient of friction for the box
 */
Box::Box(float xPos, float yPos, float width, float height, float rotation, float mass, float friction)
{
	Initialize(xPos, yPos, width, height, rotation, mass, friction);

	Type = ShapeType::BoxType;
}

/**
 * Destroy this instance of a Box object
 */
Box::~Box()
{
}

/**
 * Reinitialize instance of a Box object
 *
 * @xPos		Position of the box in the X axis
 * @yPos		Position of the box in the Y axis
 * @width		Width of the box
 * @height		Height of the box
 * @rotation	Rotation of the box in radians
 * @mass		Mass of the box
 */
void Box::Initialize(float xPos, float yPos, float width, float height, float rotation, float mass, float friction)
{
	Position.X = xPos;
	Position.Y = yPos;

	Friction = friction;

	Width = width;
	Height = height;
	Rotation = rotation;

	if (mass != FLT_MAX)
	{
		Mass = mass;
		InvMass = 1.f / Mass;
		InertiaTensor = mass * (Width * Width + Height * Height) / 12.0f;		//<<Why divided by 12? http://techhouse.brown.edu/~dmorris/projects/tutorials/inertia.tensor.summary.pdf
		InvInertiaTensor = 1.f / InertiaTensor;
	}
	else
	{
		//If the mass is set to FLT_MAX, then we don't want the box to move or be affected by any force. 
		//Set the values accordingly to allow that to happen.
		Mass = FLT_MAX;
		InvMass = 0.f;
		InertiaTensor = FLT_MAX;
		InvInertiaTensor = 0.f;
	}

	Velocity = Vector2f(0.f, 0.f);
	AngularVelocity = 0.f;
	Force = Vector2f(0.f, 0.f);
}

/**
 * Get the width and height of the Box as a Vector2f
 */
Vector2f Box::GetDimensions()
{
	return Vector2f(Width, Height);
}
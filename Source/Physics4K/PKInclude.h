////////////////////////////////////////////////////////////////////////////////
// Filename: PKInclude.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Defined to allow for exporting class/methods as a class library
#ifdef PHYSICS4K_EXPORTS
    #define PK_API __declspec(dllexport)
#else
    #define PK_API __declspec(dllimport)
#endif
////////////////////////////////////////////////////////////////////////////////
// Filename: Arbiter.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Box.h"

namespace PK
{
	class PK_API Arbiter
	{
	public:
		Arbiter();
		~Arbiter();

		void Set(Box* item1, Box* item2);
		void ApplyLaw();

	private:
		Box* box1;
		Box* box2;

		int numContacts;
	};
}
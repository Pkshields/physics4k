////////////////////////////////////////////////////////////////////////////////
// Filename: Collision.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Matrix22.h"
#include "Vector2f.h"
#include "MathXtras.h"
#include "Box.h"
#include "Contact.h"
#include "Circle.h"
#include "Shape.h"

namespace PK
{
	PK_API int OBBOBBCollision(Box* box1, Box* box2, Contact* contacts);
	PK_API int CircleCircleCollision(Circle* circle1, Circle* circle2, Contact* contacts);
	PK_API int BoxCircleCollision(Box* box, Circle* circle, Contact* contacts, bool isCircleFirst);

	/**
	* Calculate the collision data between two shapes
	*
	* @circle1		Shape 1 to try and collide with...
	* @circle2		... Shape 2, which is this
	* @contacts		Contacts array for us to store our contacts in
	*/
	static int CalculateCollision(Shape* shape1, Shape* shape2, Contact* contacts)
	{
		//Get the two shape types
		ShapeType shape1Type = shape1->GetType();
		ShapeType shape2Type = shape2->GetType();

		//OBB to OBB code
		if (shape1Type == BoxType && shape2Type == BoxType)
			return OBBOBBCollision((Box*)shape1, (Box*)shape2, contacts);

		//Box to Circle code
		else if (shape1Type == BoxType && shape2Type == CircleType)
			return BoxCircleCollision((Box*)shape1, (Circle*)shape2, contacts, false);

		//Circle to Box code
		else if (shape1Type == CircleType && shape2Type == BoxType)
			return BoxCircleCollision((Box*)shape2, (Circle*)shape1, contacts, true);

		//Circle to Circle code
		else if (shape1Type == CircleType && shape2Type == CircleType)
			return CircleCircleCollision((Circle*)shape1, (Circle*)shape2, contacts);

		//No supported collision, return such
		return 0;
	}
}
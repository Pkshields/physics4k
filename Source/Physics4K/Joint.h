////////////////////////////////////////////////////////////////////////////////
// Filename: Joint.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Shape.h"
#include "Vector2f.h"
#include "Matrix22.h"
#include "MathXtras.h"

namespace PK
{
	class PK_API Joint
	{
	public:
		Joint(Shape* box1, Shape* box2, const Vector2f anchorPoint);
		~Joint();

		void Initialize(Shape* box1, Shape* box2, const Vector2f anchorPoint);

		void Update(float deltaTime);
		void ApplyImpulse();

		void GetJointAnchors(Vector2f& box1Anchor, Vector2f& box2Anchor);

	private:
		Shape* m_box1;					/**< 1st box to join */
		Shape* m_box2;					/**< 2nd box to join */

		Vector2f m_box1Anchor;			/**< Constant distance from Box1 to anchor point */
		Vector2f m_box2Anchor;			/**< Constant distance from Box2 to anchor point */

		Vector2f m_box1RotatedAnchor;	/**< Anchor above for Box1, rotated with the current rotation of the box */
		Vector2f m_box2RotatedAnchor;	/**< Anchor above for Box2, rotated with the current rotation of the box */

		Matrix22 m_invK;				/**< K matrix for the kP = V calculation, inverted for calculating P */

		Vector2f m_bias;				/**< Current bias to be applied to the calculated impulse */
		float m_biasFactor;				/**< Factor to influence the bias by */
	};
}
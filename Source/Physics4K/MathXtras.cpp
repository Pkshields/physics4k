////////////////////////////////////////////////////////////////////////////////
// Filename: MathXtras.cpp
////////////////////////////////////////////////////////////////////////////////

#include "MathXtras.h"

using namespace PK;

/**
 * ABS a Vector2f
 *
 * @value	Vector to ABS
 */
Vector2f PK::Abs(const Vector2f& value)
{
	return Vector2f(fabsf(value.X), fabsf(value.Y));
}

/**
 * ABS a Matrix22
 *
 * @value	Matrix22 to ABS
 */
Matrix22 PK::Abs(const Matrix22& value)
{
	return Matrix22(fabsf(value.Column1.X), fabsf(value.Column1.Y), fabsf(value.Column2.X), fabsf(value.Column2.Y));
}

/**
 * Calculate just the sign of a float
 *
 * @value	Float to test
 */
float PK::Sign(float value)
{
	return value < 0.0f ? -1.0f : 1.0f;
}

/**
 * Calculate the dot product of two vectors
 *
 * @vectorA		Vector to test
 * @vectorB		Another vector to test
 */
float PK::Dot(const Vector2f& vectorA, const Vector2f& vectorB)
{
	return vectorA.X * vectorB.X + vectorA.Y * vectorB.Y;
}

/**
 * Calculate the tangent vector of a given 2D vector (rotate by 90 degrees)
 *
 * @vectorA		Vector to rotate
 */
Vector2f PK::Tangent(const Vector2f& vector)
{
	return Vector2f(vector.Y, -vector.X);
}

/**
 * Calculate the weird 2D cross product thing
 *
 * @v1		Part 1 of the two vector equation
 * @v2		Part 2 of the two vector equation
 */
float PK::Cross(const Vector2f& v1, const Vector2f& v2)
{
	return v1.X * v2.Y - v1.Y * v2.X;
}

/**
 * Clamp a particlualr value between two numbers
 *
 * @value		Value to clamp
 * @min			Minimum value
 * @max			Maximum value
 */
float PK::Clamp(float value, const float min, const float max)
{
	value = (value < min ? min : value);
	value = (value > max ? max : value);
	return value;
}

/**
 * Apply the factorial (!) to a number
 *
 * @n		Number to Factorialize
 */
unsigned int PK::Factorial(unsigned int n)
{
	unsigned int final = 1;
	for (unsigned int i = 1; i <= n; i++)
		final *= i;
	return final;
}

/**
 * Get the triangle number for a specific part of the sequence
 *
 * @n		Number to triangleize
 */
unsigned int PK::TriangleNumber(unsigned int n)
{
	unsigned int final = 0;
	for (unsigned int i = 1; i <= n; i++)
		final += i;
	return final;
}

/**
 * Fast inverse square root using Newton-Rapheson approximation. Straight Quake 3 right here
 * Taken from: http://forums.overclockers.co.uk/showthread.php?p=8773984 and http://en.wikipedia.org/wiki/Fast_inverse_square_root
 *
 * @n		Number to rootify
 */
float PK::FastInvSqrt(float n)
{
	float nHalf = 0.5f * n;
	int i = *(int*)&n;				//Get bits for floating value
	i = 0x5f375a86 - (i >> 1);		//Gives initial guess y0
	n = *(float*)&i;				//Convert bits back to float
	n = n * (1.5f - nHalf * n * n);	//Newton step, repeating increases accuracy
	return n;
}

/**
 * Fast square root using the fast inverse square root method
 *
 * @n		Number to rootify
 */
float PK::FastSqrt(float n)
{
	//sqrt(f) == 1 / invsqrt(f) == f * invsqrt(f)
	return n * FastInvSqrt(n);
}
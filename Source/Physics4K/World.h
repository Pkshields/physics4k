////////////////////////////////////////////////////////////////////////////////
// Filename: World.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include <vector>
#include "Box.h"
#include "Vector2f.h"
#include "ShapePair.h"
#include "MathXtras.h"
#include "Joint.h"
#include "Shape.h"

namespace PK
{
	class PK_API World
	{
	public:
		World(Vector2f gravity);
		~World();

		void GenerateWorld();
		bool Update(float timeStep);

		void Add(Shape* item);
		void Add(Joint* item);
		Joint* GenerateJoint(Box* box1, Box* box2, const Vector2f anchorPoint);
		void Clear();

		std::vector<Vector2f> GetCurrentCollisionPoints();

	private:
		std::vector<Shape*> m_physicsList;	/**< List of all the physics object in the world */
		std::vector<Joint*> m_jointList;	/**< List of all the joints in the world */
		ShapePair** m_shapePairList;		/**< Array of ShapePairs, containing all the boxes above */
		int m_shapePairListSize;			/**< Size of above array */

		bool m_canRun;						/**< Is the World "built"? */
		Vector2f m_gravity;					/**< Value for the gravity. */
	};
}
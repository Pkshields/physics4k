////////////////////////////////////////////////////////////////////////////////
// Filename: BoxPair.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Box.h"
#include "Contact.h"
#include "Collision.h"

namespace PK
{
	class PK_API BoxPair
	{
		friend class World;

	public:
		BoxPair(Box* box1, Box* box2);
		~BoxPair();

		void Update(float deltaTime);
		void ApplyImpulse();

	private:
		void CalculateContactData(float deltaTime);

	private:
		Box* m_box1;
		Box* m_box2;

		float combinedFriction;

		Contact m_contacts[2];
		int m_numContacts;
	};
}
////////////////////////////////////////////////////////////////////////////////
// Filename: Arbiter.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Arbiter.h"

using namespace PK;

/**
 * Create an instance of the arbiter of all things physics
 */
Arbiter::Arbiter()
{
	box1 = 0;
	box2 = 0;
	numContacts = 0;
}

/**
 * Destroy this instance of the arbiter of all things physics
 */
Arbiter::~Arbiter()
{
}

/**
 * Change the items that the arbiter are manipulating
 *
 * @item1 Item 1 to check
 * @item2 Item 2 to check
 */
void Arbiter::Set(Box* item1, Box* item2)
{
	box1 = item1;
	box2 = item2;
	numContacts = 0;
}

/**
 * Apply the brutalities of physics law to both of the objects in the buffer
 */
void ApplyLaw()
{
	//Find the number of contact points between the two objects
	
}
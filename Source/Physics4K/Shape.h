////////////////////////////////////////////////////////////////////////////////
// Filename: Shape.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Vector2f.h"
#include <float.h>

namespace PK
{
	//Enum containing all supported shape types
	enum ShapeType
	{
		BoxType,
		CircleType,
		NoneShape
	};

	class PK_API Shape
	{
		friend class World;

	protected:		//Abstract class. no destructor needed?
		Shape();
		Shape(float xPos, float yPos, float rotation, float friction, float mas, float intertiaTensor);

		void ZeroForce();

	public:
		void SetForce(Vector2f force);
		ShapeType GetType();

	public:
		Vector2f Position;		/**< Position of the shape */
		float Rotation;			/**< Rotation of the shape in radians */

		float Friction;			/**< Coefficient of friction for the object */

		float Mass;				/**< Mass of the shape */
		float InvMass;			/**< 1 / Mass of the shape (used for calculations) */
		float InertiaTensor;	/**< Density of an object (http://techhouse.brown.edu/~dmorris/projects/tutorials/inertia.tensor.summary.pdf) */
		float InvInertiaTensor;	/**< 1 / the Inertia Tensor (used for calculations) */

		Vector2f Velocity;		/**< Current velocity of the shape */
		float AngularVelocity;	/**< Current angular velocity of the shape */
		Vector2f Force;			/**< External force to be applied to the system */

	protected:
		ShapeType Type;			/**< Type of shape this shape is */
	};
}
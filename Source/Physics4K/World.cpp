////////////////////////////////////////////////////////////////////////////////
// Filename: World.h
////////////////////////////////////////////////////////////////////////////////

#include "World.h"

using namespace PK;

/**
 * Create an instance of the physics world
 */
World::World(Vector2f gravity)
{
	m_gravity = gravity;
	m_canRun = false;
	m_shapePairList = 0;
	m_shapePairListSize = 0;
}

/**
 * Destroy this instance of the physics world
 */
World::~World()
{
	//Delete all boxes in the Physics List
	int listSize = (int)m_physicsList.size();
	for (int i = 0; i < listSize - 1; i++)
	{
		delete m_physicsList[i];
	}

	//Delete all joints in the Physics List
	listSize = (int)m_jointList.size();
	for (int i = 0; i < listSize - 1; i++)
	{
		delete m_jointList[i];
	}

	//Delete all of the pairs in the pair array
	if (m_shapePairList)
	{
		for (int i = 0; i < m_shapePairListSize; i++)
			delete m_shapePairList[i];
		delete[] m_shapePairList;
	}
}

/**
 * Generate the physics wold
 */
void World::GenerateWorld()
{
	//If the boxPairList is not already empty, delete everything in it
	if (m_shapePairList != 0)
	{
		for (int i = 0; i < m_shapePairListSize; i++)
			delete m_shapePairList[i];
		delete[] m_shapePairList;
	}

	//Generate the new boxPairList
	m_shapePairListSize = TriangleNumber(m_physicsList.size() - 1);
	m_shapePairList = new ShapePair*[m_shapePairListSize];

	//For each of the items in the physics list
	int listSize = (int)m_physicsList.size();
	Shape *item1, *item2;
	int currentBoxPair = 0;
	for (int i = 0; i < listSize - 1; i++)
	{
		//Get an item
		item1 = m_physicsList[i];

		//Check it with each of the items in the list ahead of itself
		for (int j = i + 1; j < listSize; j++)
		{
			//Get the second item!
			item2 = m_physicsList[j];

			//Generate a BoxPair for both of these
			ShapePair* pair = new ShapePair(item1, item2);
			m_shapePairList[currentBoxPair] = pair;
			currentBoxPair++;
		}
	}

	m_canRun = true;
}

/**
 * Update the physics wold
 */
bool World::Update(float timeStep)
{
	//If the game is not generated, then just return an error
	if (!m_canRun)
		return false;

	//Convert the milliseconds into seconds
	timeStep /= 1000.f;

	//Calculate the number of objects in the world and their pairs
	int numOfBoxes = m_physicsList.size();
	int numOfBoxPairs = m_shapePairListSize;
	int numOfJoints = m_jointList.size();

	//Apply gravity and the external force to all the objects
	for (int i = 0; i < numOfBoxes; i++)
		if (m_physicsList[i]->Mass != FLT_MAX)
		{
			Shape* box = m_physicsList[i];
			box->Velocity += m_gravity * timeStep;
			box->Velocity += (box->Force / box->Mass) * timeStep;
		}

	//Calculate the collision data for all of the objects in the world
	for (int i = 0; i < numOfBoxPairs; i++)
		m_shapePairList[i]->Update(timeStep);

	//Calculate the K matrix and everything else the joint needs before the ApplyImpulse method
	for (int i = 0; i < numOfJoints; i++)
		m_jointList[i]->Update(timeStep);

	//Iterate through the ApplyImpulse to help with the jitter
	for (int j = 0; j < 15; j++)
	{
		//Apply the impulse to each of the objects in the BoxPair list
		for (int i = 0; i < numOfBoxPairs; i++)
			m_shapePairList[i]->ApplyImpulse();

		//Apply the impulse from the joints to the boxes
		for (int i = 0; i < numOfJoints; i++)
			m_jointList[i]->ApplyImpulse();
	}

	//Apply the velocity for all the objects to their own positions
	for (int i = 0; i < numOfBoxes; i++)
	{
		m_physicsList[i]->Position += m_physicsList[i]->Velocity * timeStep;
		m_physicsList[i]->Rotation += m_physicsList[i]->AngularVelocity * timeStep;
		m_physicsList[i]->ZeroForce();	//While we are here, reset the external force to zero
	}
	return true;
}

/**
 * Add an item to be affected by physics to the physics world
 *
 * @item	Item to add
 */
void World::Add(Shape* item)
{
	m_physicsList.push_back(item);
	m_canRun = false;
}

/**
 * Add a joint to affect the boxes the physics world
 *
 * @item	Joint to add
 */
void World::Add(Joint* item)
{
	m_jointList.push_back(item);
	m_canRun = false;
}

/**
 * Generate a joint between two boxes
 *
 * @box1			Box 1 to join to...
 * @box2			... Box 2, which is this
 * @anchorPoint		Point to anchor them both to, part they can rotate around
 */
Joint* World::GenerateJoint(Box* box1, Box* box2, const Vector2f anchorPoint)
{
	//Create the joint with the parameters, add it to the jointList and return the pointer tot he joint for all to see (if they want it)
	Joint* joint = new Joint(box1, box2, anchorPoint);
	m_jointList.push_back(joint);
	m_canRun = false;
	return joint;
}

/**
 * Clear all the objects out of the physics world
 */
void World::Clear()
{
	m_physicsList.clear();
	m_jointList.clear();
	m_canRun = false;
}

/**
 * Get all current collision points from the BoxPairs
 */
std::vector<Vector2f> World::GetCurrentCollisionPoints()
{
	//Create a list to build
	std::vector<Vector2f> list = std::vector<Vector2f>();

	//Loop through all the boxPairs. If a BoxPair has contact points, add them to the list
	for (unsigned int i = 0; i < m_shapePairListSize; i++)
		if (m_shapePairList[i]->m_numContacts != 0)
			for (unsigned int j = 0; j < m_shapePairList[i]->m_numContacts; j++)
				list.push_back(m_shapePairList[i]->m_contacts[j].Position);

	//Return the list
	return list;
}
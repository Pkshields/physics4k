////////////////////////////////////////////////////////////////////////////////
// Filename: Contact.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Contact.h"

using namespace PK;

/**
 * Create an instance of the Contact object
 */
Contact::Contact()
{
	Position = Vector2f();
	Normal = Vector2f();
	Separation = 0.f;
	RIMNormal = 0.f;
	RIMTangent = 0.f;
	PenetrationOffset = 0.f;
}

/**
 * Create an instance of the Contact object
 *
 * @pos		Position of the contact
 * @nor		Normal for the contact
 * @sep		Separation of the contact from the incident face
 */
Contact::Contact(Vector2f pos, Vector2f nor, float sep)
{
	Position = pos;
	Normal = nor;
	Separation = sep;
	RIMNormal = 0.f;
	RIMTangent = 0.f;
	PenetrationOffset = 0.f;
}

/**
 * Destroy this instance of the Contact object
 */
Contact::~Contact()
{
}

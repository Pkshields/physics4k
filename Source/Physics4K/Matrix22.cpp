////////////////////////////////////////////////////////////////////////////////
// Filename: Matrix22.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Matrix22.h"

using namespace PK;

/**
 * Constructor for the Matrix22
 */
Matrix22::Matrix22()
{
	Column1.X = 0.f;	Column2.X = 0.f;
	Column1.Y = 0.f;	Column2.Y = 0.f;
}

/**
 * Constructor for the Matrix22
 *
 * @angle	Constructs a rotation matrix using this angle (radians)
 */
Matrix22::Matrix22(float angle)
{
	float cos = cosf(angle), sin = sinf(angle);
	Column1.X = cos; Column2.X = -sin;
	Column1.Y = sin; Column2.Y = cos;
}

/**
 * Constructor for the Matrix22
 *
 * @col11		Value in the Column 1 Atom 1
 * @col12		Value in the Column 1 Atom 2
 * @col21		Value in the Column 2 Atom 1
 * @col22		Value in the Column 2 Atom 2
 */
Matrix22::Matrix22(float col11, float col12, float col21, float col22)
{
	Column1.X = col11; Column2.X = col21;
	Column1.Y = col12; Column2.Y = col22;
}

/**
 * Constructor for the Matrix22
 *
 * @col1	Value in the Column 1
 * @col2	Value in the Column 2
 */
Matrix22::Matrix22(Vector2f col1, Vector2f col2)
{
	Column1 = col1;
	Column2 = col2;
}

/**
 * Constructor for the Matrix22
 *
 * @copy	Matrix to copy
 */
Matrix22::Matrix22(const Matrix22& copy)
{
	Column1.X = copy.Column1.X; Column2.X = copy.Column2.X;
	Column1.Y = copy.Column1.Y; Column2.Y = copy.Column2.Y;
}

/**
 * Destructor for the Vector2f
 */
Matrix22::~Matrix22()
{
}

/**
 * Transpose this matrix
 */
void Matrix22::Transpose()
{
	float temp1;
	temp1 = Column2.X;
	Column2.X = Column1.Y;
	Column1.Y = temp1;
}

/**
 * Transpose this matrix
 *
 * @mat		Matric to put the transpose into
 */
void Matrix22::Transpose(Matrix22& mat)
{
	mat = Matrix22(Column1, Column2);

	mat.Column2.X = Column1.Y;
	mat.Column1.Y = Column2.X;
}

/**
 * Invert this matrix
 */
bool Matrix22::Invert()
{
	//Calculate the determinate
	float a = Column1.X, 
		  b = Column2.X, 
		  c = Column1.Y, 
		  d = Column2.Y;
	float det = a * d - b * c;

	//Check, ensuring the determinate is not zero. Return false if it is.
	if (det == 0.f)
		return false;

	//Get the inverse of the determinant, which is required for inverting
	det = 1.f / det;

	//Calculate the new inverted matrix
	Column1.X =  det * d;	Column2.X = -det * b;
	Column1.Y = -det * c;	Column2.Y =  det * a;

	return true;
}

/**
 * = operator for the Matrix22
 *
 * @param	Matrix22 to copy
 */
Matrix22& Matrix22::operator=(const Matrix22 &param)
{
	if (this == &param)
      return *this;

	Column1.X = param.Column1.X;
	Column2.X = param.Column2.X;
	Column1.Y = param.Column1.Y;
	Column2.Y = param.Column2.Y;

	return *this;
}

/**
 * += operator for the Matrix22
 *
 * @param	Matrix22 to add
 */
Matrix22& Matrix22::operator+= (const Matrix22 &param)
{
	Column1.X += param.Column1.X;
	Column2.X += param.Column2.X;
	Column1.Y += param.Column1.Y;
	Column2.Y += param.Column2.Y;

	return *this;
}

/**
 * -= operator for the Matrix22
 *
 * @param	Matrix22 to subtract
 */
Matrix22& Matrix22::operator-= (const Matrix22 &param)
{
	Column1.X -= param.Column1.X;
	Column2.X -= param.Column2.X;
	Column1.Y -= param.Column1.Y;
	Column2.Y -= param.Column2.Y;

	return *this;
}

/**
 * *= operator for the Matrix22
 *
 * @param	Matrix to multiple to multiply
 */
Matrix22& Matrix22::operator*= (const Matrix22 &param)
{
	Vector2f col1(Column1), col2(Column2);

	Column1.X = (col1.X * param.Column1.X) + (col2.X * param.Column1.Y);
	Column2.X = (col1.X * param.Column2.X) + (col2.X * param.Column2.Y);
	Column1.Y = (col1.Y * param.Column1.X) + (col2.Y * param.Column1.Y);
	Column2.Y = (col1.Y * param.Column2.X) + (col2.Y * param.Column2.Y);

	return *this;
}

/**
 * *= operator for the Matrix22
 *
 * @param	Scalar to multiple by
 */
Matrix22& Matrix22::operator*= (const float &param)
{
	Column1.X = Column1.X * param;
	Column2.X = Column2.X * param;
	Column1.Y = Column1.Y * param;
	Column2.Y = Column2.Y * param;

	return *this;
}

/**
 * + operator for the Matrix22
 *
 * @param	Matrix22 to add
 */
const Matrix22 Matrix22::operator+ (const Matrix22 &param) const
{
	Matrix22 result = *this; 
	result += param;
	return result;
}

/**
 * - operator for the Matrix22
 *
 * @param	Matrix22 to subtract
 */
const Matrix22 Matrix22::operator- (const Matrix22 &param) const
{
	Matrix22 result = *this; 
	result -= param;
	return result;
}

/**
 * * operator for the Matrix22
 *
 * @param	Matrix22 to multiply into
 */
const Matrix22 Matrix22::operator* (const Matrix22 &param) const
{
	Matrix22 result = *this; 
	result *= param;
	return result;
}

/**
 * * operator for the Matrix22
 *
 * @param	Scalar to multiply into
 */
const Matrix22 Matrix22::operator* (const float &param) const
{
	Matrix22 result = *this; 
	result *= param;
	return result;
}

/**
 * * a matrix into the vector, rotation style
 *
 * @param	Vector to rotate... or something else?
 */
const Vector2f Matrix22::operator* (const Vector2f &param)const 
{
	return Vector2f((Column1.X * param.X) + (Column2.X * param.Y), 
					(Column1.Y * param.X) + (Column2.Y * param.Y));
}
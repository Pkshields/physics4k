////////////////////////////////////////////////////////////////////////////////
// Filename: CollisionBoxBox.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Collision.h"

enum Axis
{
	FACE_A_X,
	FACE_A_Y,
	FACE_B_X,
	FACE_B_Y
};

#pragma region PREDEFINITION

int ClipSegmentToLine(Vector2f vOut[2], Vector2f vIn[2], const Vector2f& normal, float offset);
static void ComputeIncidentEdge(Vector2f c[2], const Vector2f& h, const Vector2f& pos, const Matrix22& Rot, const Vector2f& normal);

#pragma endregion

/**
 * Calculate the Box to Box collision
 *
 * @box1		Box 1 to check
 * @box2		Box 1 to check
 * @contacts	Array of contacts to return
 */
int PK::OBBOBBCollision(Box* box1, Box* box2, Contact* contacts)
{
	///		PART 1: Setup
	//Calculate the 1/2 widths of the boxes
	Vector2f hA = box1->GetDimensions() * 0.5f;
	Vector2f hB = box2->GetDimensions() * 0.5f;

	//Get the positions of the boxes
	Vector2f posA = box1->Position;
	Vector2f posB = box2->Position;

	//Calculate the rotation matrixes from the box rotation values
	Matrix22 RotA(box1->Rotation), 
			 RotB(box2->Rotation);

	//Calculate the transpose of the above matrices
	Matrix22 RotAT = Matrix22(RotA);
	RotAT.Transpose();
	Matrix22 RotBT = Matrix22(RotB);
	RotBT.Transpose();

	//Pull the colums out of the rotation matrixes for easy normal access
	Vector2f a1 = RotA.Column1, a2 = RotA.Column2;
	Vector2f b1 = RotB.Column1, b2 = RotB.Column2;

	//Calculate the differnce vector between the centers of both fo the boxes
	//Then rotate that into the local spaces of both of the boxes
	Vector2f dp = posB - posA;
	Vector2f dA = RotAT * dp;
	Vector2f dB = RotBT * dp;

	//
	Matrix22 C = RotAT * RotB;
	Matrix22 absC = Abs(C);
	Matrix22 absCT = Matrix22(absC);
	absCT.Transpose();

	///		PART 2: SEPARATING AXIS TEST

	//Calculate the separating axis test for the two axis taken from Box A
	Vector2f faceA = Abs(dA) - hA - absC * hB;
	if (faceA.X > 0.0f || faceA.Y > 0.0f)		//If either of the penetration values are > 0, then there is no penetration. Exit out of collision
		return 0;

	//Calculate the separating axis test for the two axis taken from Box B
	Vector2f faceB = Abs(dB) - absCT * hA - hB;
	if (faceB.X > 0.0f || faceB.Y > 0.0f)		//If either of the penetration values are > 0, then there is no penetration. Exit out of collision
		return 0;

	///		PART 3: FIND THE AXIS WITH LEAST PENETRATION
	Axis axis;
	float separation;
	Vector2f normal;

	//Set the initial value as Box A, face facing in the X axis
	axis = FACE_A_X;
	separation = faceA.X;
	normal = dA.X > 0.0f ? RotA.Column1 : RotA.Column1 * -1;	//Determine which face in the X direction is closest to the other box using
																// the difference vector rotated into the local space of Box A

	//Check the other 3 faces. If the penetration distance is smaller than the one we have currently stored, then we use that distance and 
	// face as the reference face

	//Box A, face facing in the Y axis
	if (faceA.Y > separation)
	{
		axis = FACE_A_Y;
		separation = faceA.Y;
		normal = dA.Y > 0.0f ? RotA.Column2 : RotA.Column2 * -1;
	}

	//Box B, face facing in the X axis
	if (faceB.X > separation)
	{
		axis = FACE_B_X;
		separation = faceB.X;
		normal = dB.X > 0.0f ? RotB.Column1 : RotB.Column1 * -1;
	}

	//Box B, face facing in the Y axis
	if (faceB.Y > separation)
	{
		axis = FACE_B_Y;
		separation = faceB.Y;
		normal = dB.Y > 0.0f ? RotB.Column2 : RotB.Column2 * -1;
	}

	///		PART 4: SET UP THE CLIPPING PLANE DATA BASED UPON THE REFERENCE PLANE
	///				CALCULATE THE INCIDENT PLANE
	///				THIS IS THE BIT I'M MOST FUZZY ON. HOW DOES IT?

	// Setup clipping plane data based on the separating axis
	Vector2f frontNormal, sideNormal;
	Vector2f incidentEdge[2];
	float front, negSide, posSide;

	// Compute the clipping lines and the line segment to be clipped.
	switch (axis)
	{
	case FACE_A_X:
		{
			frontNormal = normal;					//Normal for the 
			front = Dot(posA, frontNormal) + hA.X;
			sideNormal = RotA.Column2;
			float side = Dot(posA, sideNormal);
			negSide = -side + hA.Y;
			posSide =  side + hA.Y;
			ComputeIncidentEdge(incidentEdge, hB, posB, RotB, frontNormal);
		}
		break;

	case FACE_A_Y:
		{
			frontNormal = normal;
			front = Dot(posA, frontNormal) + hA.Y;
			sideNormal = RotA.Column1;
			float side = Dot(posA, sideNormal);
			negSide = -side + hA.X;
			posSide =  side + hA.X;
			ComputeIncidentEdge(incidentEdge, hB, posB, RotB, frontNormal);
		}
		break;

	case FACE_B_X:
		{
			frontNormal = normal * -1;
			front = Dot(posB, frontNormal) + hB.X;
			sideNormal = RotB.Column2;
			float side = Dot(posB, sideNormal);
			negSide = -side + hB.Y;
			posSide =  side + hB.Y;
			ComputeIncidentEdge(incidentEdge, hA, posA, RotA, frontNormal);
		}
		break;

	case FACE_B_Y:
		{
			frontNormal = normal * -1;
			front = Dot(posB, frontNormal) + hB.Y;
			sideNormal = RotB.Column1;
			float side = Dot(posB, sideNormal);
			negSide = -side + hB.X;
			posSide =  side + hB.X;
			ComputeIncidentEdge(incidentEdge, hA, posA, RotA, frontNormal);
		}
		break;
	}

	///		PART 5: CLIP THE INCIDENT PLANE AGAINST REFERENCE PLANE SIDE PLANES

	Vector2f clipPoints1[2];
	Vector2f clipPoints2[2];
	int numberPoints;

	//Clip to the first side plane, the negative side plane
	numberPoints = ClipSegmentToLine(clipPoints1, incidentEdge, sideNormal * -1, negSide);

	//If we have less than 2 end points to the plane/line returned, then we have clipped too much and somethign has went wrong. Return 0
	//Purely a failsafe, never has actually run.
	if (numberPoints < 2)
		return 0;

	//Clip to the second side plane, the positive side plane
	numberPoints = ClipSegmentToLine(clipPoints2, clipPoints1,  sideNormal, posSide);

	//Same as above
	if (numberPoints < 2)
		return 0;

	//Generate the collision contact data to return to the resolver
	int numContacts = 0;
	for (int i = 0; i < 2; i++)
	{
		//Calcalate the penetration distance for each contact point to the surface of the reference plane
		float separation = Dot(frontNormal, clipPoints2[i]) - front;

		//Last check to make sure we have penetration
		//With the way the clipping works, it doesn't clip to the reference plane itself, so one of the points may not be colliding at all.
		if (separation <= 0)
		{
			//Store the spearation, normal of the collision and the actual position of the contac point
			//We slide the contacT point onto the reference plane by adding onthe separation in the drection of the normal
			contacts[numContacts].Separation = separation;
			contacts[numContacts].Normal = normal;
			contacts[numContacts].Position = clipPoints2[i] - (frontNormal * separation);
			numContacts++;
		}
	}

	return numContacts;
}


/**
 * Clip a line segment to a line
 */
int ClipSegmentToLine(Vector2f vOut[2], Vector2f vIn[2], const Vector2f& normal, float offset)
{
	// Start with no output points
	int numOut = 0;

	// Calculate the distance of end points to the line
	float distance0 = Dot(normal, vIn[0]) - offset;
	float distance1 = Dot(normal, vIn[1]) - offset;

	// If the points are behind the plane
	if (distance0 <= 0.0f) vOut[numOut++] = vIn[0];
	if (distance1 <= 0.0f) vOut[numOut++] = vIn[1];

	// If the points are on different sides of the plane
	if (distance0 * distance1 < 0.0f)
	{
		// Find intersection point of edge and plane
		float interp = distance0 / (distance0 - distance1);
		vOut[numOut] = vIn[0] + (vIn[1] - vIn[0]) * interp;
		++numOut;
	}

	return numOut;
}

/**
 * Calculate the incident edge of the second box, given the reference edge
 */
static void ComputeIncidentEdge(Vector2f c[2], const Vector2f& h, const Vector2f& pos, const Matrix22& Rot, const Vector2f& normal)
{
	// The normal is from the reference box. Convert it
	// to the incident boxe's frame and flip sign.
	Matrix22 RotT = Matrix22(Rot);
	RotT.Transpose();
	Vector2f n = (RotT * normal) * -1;
	Vector2f nAbs = Abs(n);

	if (nAbs.X > nAbs.Y)
	{
		if (Sign(n.X) > 0.0f)
		{
			c[0] = Vector2f(h.X, -h.Y);
			c[1] = Vector2f(h.X, h.Y);
		}
		else
		{
			c[0] = Vector2f(-h.X, h.Y);
			c[1] = Vector2f(-h.X, -h.Y);
		}
	}
	else
	{
		if (Sign(n.Y) > 0.0f)
		{
			c[0] = Vector2f(h.X, h.Y);
			c[1] = Vector2f(-h.X, h.Y);
		}
		else
		{
			c[0] = Vector2f(-h.X, -h.Y);
			c[1] = Vector2f(h.X, -h.Y);
		}
	}

	c[0] = pos + Rot * c[0];
	c[1] = pos + Rot * c[1];
}
////////////////////////////////////////////////////////////////////////////////
// Filename: ShapePair.cpp
////////////////////////////////////////////////////////////////////////////////

#include "ShapePair.h"

using namespace PK;

/**
 * Create an instance of a Box Pair
 *
 * @box1	1st box to add
 * @box2	2nd box to add
 */
ShapePair::ShapePair(Shape* box1, Shape* box2)
{
	m_box1 = box1;
	m_box2 = box2;
	m_numContacts = 0;
	combinedFriction = sqrtf(box1->Friction * box1->Friction);	//Calculate the average friction between the two objects
						//Uses the default sqrt for accuracy, as it only runs once
}

/**
 * Destroy this instance of a Box Pair
 */
ShapePair::~ShapePair()
{
}

/**
 * 
 */
void ShapePair::Update(float deltaTime)
{
	//Calculate the collision contact points between the two boxes
	m_numContacts = CalculateCollision(m_box1, m_box2, m_contacts);

	//Calculate the contact data for each of the contact points
	CalculateContactData(deltaTime);
}

/**
 * 
 */
void ShapePair::CalculateContactData(float deltaTime)
{
	//CONST
	const float allowedPenetration = 0.35f;

	//For all the contacts that we are using in the array
	for (int i = 0; i < m_numContacts; ++i)
	{
		//Calculate the position of the boxes relative to the contact point
		Vector2f box1Relative = m_contacts[i].Position - m_box1->Position;
		Vector2f box2Relative = m_contacts[i].Position - m_box2->Position;

		//Calculate the Reaction Impulse Magnitude for this contact point
		//Info taken from: http://en.wikipedia.org/wiki/Collision_response#Impulse-Based_Reaction_Model
		//We calculate the RIM across both the normal (for the collision response force) and the tangent (for the friction to be applied)
		//These calculations are for the denominator of the equation shown in the link, the static part that does not depend on the velocity

		//Normal
		float rn1 = Dot(box1Relative, m_contacts[i].Normal);
		float rn2 = Dot(box2Relative, m_contacts[i].Normal);
		m_contacts[i].RIMNormal = m_box1->InvMass + m_box2->InvMass;
		m_contacts[i].RIMNormal += m_box1->InvInertiaTensor * (Dot(box1Relative, box1Relative) - rn1 * rn1);
		m_contacts[i].RIMNormal += m_box2->InvInertiaTensor * (Dot(box2Relative, box2Relative) - rn2 * rn2);
		m_contacts[i].RIMNormal = 1.0f / m_contacts[i].RIMNormal;

		//Tangent
		Vector2f tangent = Tangent(m_contacts[i].Normal);
		float rt1 = Dot(box1Relative, tangent);
		float rt2 = Dot(box2Relative, tangent);
		m_contacts[i].RIMTangent = m_box1->InvMass + m_box2->InvMass;
		m_contacts[i].RIMTangent += m_box1->InvInertiaTensor * (Dot(box1Relative, box1Relative) - rt1 * rt1);
		m_contacts[i].RIMTangent += m_box2->InvInertiaTensor * (Dot(box2Relative, box2Relative) - rt2 * rt2);
		m_contacts[i].RIMTangent = 1.0f / m_contacts[i].RIMTangent;

		//As the objects are penetrating each other (unlike real life) we need to apply a velocity to the objects to move them back out
		//Calculated here is the velocity travelled total by the objects from then they just collided to where they are now.
		//We use this, combined with the bounce factor in ApplyImpulse to move the objects out of each other.
		float separationAfterAllow = m_contacts[i].Separation + allowedPenetration;
		separationAfterAllow = (separationAfterAllow > 0.f ? 0.f : separationAfterAllow);	/********************/
		m_contacts[i].PenetrationOffset = -1 * (separationAfterAllow / deltaTime);
	}
}

/**
 * Apply impulse from both boxes in the BoxPair to wach other
 */
void ShapePair::ApplyImpulse()
{
	//CONST
	const float bounceFactor = .2f;

	for (int i = 0; i < m_numContacts; i++)
	{
		//Calculate the position of the boxes relative to the contact point
		Vector2f box1Relative = m_contacts[i].Position - m_box1->Position;
		Vector2f box2Relative = m_contacts[i].Position - m_box2->Position;

		//Calculate the tangents to both of the relative positions
		Vector2f box1RelativeTangent = Tangent(box1Relative);
		Vector2f box2RelativeTangent = Tangent(box2Relative);

		//Calculate the relative velocity at contact point for each of the objects
		//How is the AngularVelocity >> Linear Velocity  conversion done?
		// Thinking about angular velocity, if a box was rotating clockwise (+ive) and hits thro ground, the rotation would cause a linear
		// velocity to to the left. So we use the relative position of each of the boxes to the contact point, find the tangent to that and
		// multiply that by the angular velocity.
		//AngularTangent is negated here because the tangent calculated is in the opposite direction to what we need (we need to the right)
		Vector2f relativeVelocity = m_box2->Velocity + (box2RelativeTangent * -m_box2->AngularVelocity) - m_box1->Velocity - (box1RelativeTangent * -m_box1->AngularVelocity);

		//Calculating Impulse Based Reaction
		//	NORMAL
		//Calculate the Numerator for the Reaction Impulse Magnitude and apply it to the denuminator
		float speedNormal = Dot(relativeVelocity, m_contacts[i].Normal);													//Calculate the relative speed
		float rimNumerator = m_contacts[i].RIMNormal * (-speedNormal + (m_contacts[i].PenetrationOffset * bounceFactor));
		
		//If rimNumerator is less than 0 then the force applied to the objects would force them into each other.
		// Stop impulse.
		if (rimNumerator < 0.f)
			rimNumerator = 0.f;

		//Calculate the Reaction Impulse Vector
		//Part 2 of http://en.wikipedia.org/wiki/Collision_response#Computing_Impulse-Based_Reaction
		Vector2f riv = rimNumerator * m_contacts[i].Normal;

		//Apply the RIV to the velocity of each of the boxes
		//Both of these equations taken from the link above
		m_box1->Velocity -= m_box1->InvMass * riv;
		m_box1->AngularVelocity -= m_box1->InvInertiaTensor * Cross(box1Relative, riv);
		m_box2->Velocity += m_box2->InvMass * riv;
		m_box2->AngularVelocity += m_box2->InvInertiaTensor * Cross(box2Relative, riv);

		//Recalculate the relative velocity at contact point for each of the objects
		//Velocity has now changed and can now be zero. If it is, then at the speedTangent line the calculated friction should be zero. If we don't recalculate, 
		// then it would be calcualted as something small, but still something. then that something small would be applied, makign the next loop something now zero, keeping the boxes moving
		relativeVelocity = m_box2->Velocity + (box2RelativeTangent * -m_box2->AngularVelocity) - m_box1->Velocity - (box1RelativeTangent * -m_box1->AngularVelocity);

		//	TANGENT (Friction)
		//Calculate the tangent from the normal
		Vector2f tangent = Tangent(m_contacts[i].Normal);

		//Calculate the Numerator for the Reaction Impulse Magnitude and apply it to the denuminator
		float speedTangent = Dot(relativeVelocity, tangent);
		float rimTangentNumerator = m_contacts[i].RIMTangent * (-speedTangent);

		//Calculate the maximum values for friction from the combined friction coefficients and make ssure the speed value to be applied is within the max values
		float maxFriction = combinedFriction * rimNumerator;
		rimTangentNumerator = Clamp(rimTangentNumerator, -maxFriction, maxFriction);

		//Calculate the Reaction Impulse Vector for the friction
		//Part 2 of http://en.wikipedia.org/wiki/Collision_response#Computing_Impulse-Based_Reaction
		riv = rimTangentNumerator * tangent;

		//Apply the RIV to the velocity of each of the boxes
		//Both of these equations taken from the link above
		m_box1->Velocity -= m_box1->InvMass * riv;
		m_box1->AngularVelocity -= m_box1->InvInertiaTensor * Cross(box1Relative, riv);
		m_box2->Velocity += m_box2->InvMass * riv;
		m_box2->AngularVelocity += m_box2->InvInertiaTensor * Cross(box2Relative, riv);
	}
}
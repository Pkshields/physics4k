////////////////////////////////////////////////////////////////////////////////
// Filename: Box.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Vector2f.h"
#include <float.h>
#include "Shape.h"

namespace PK
{
	class PK_API Box : public Shape
	{
		friend class World;

	public:
		Box(float xPos, float yPos, float width, float height, float rotation, float mass);
		Box(float xPos, float yPos, float width, float height, float rotation, float mass, float friction);
		~Box();

		void Initialize(float xPos, float yPos, float width, float height, float rotation, float mass, float friction = 0.2f);

		Vector2f GetDimensions();

	public:
		float Width;			/**< Width of the box */
		float Height;			/**< Height of the box */
	};
}
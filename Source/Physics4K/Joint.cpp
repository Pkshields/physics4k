////////////////////////////////////////////////////////////////////////////////
// Filename: Joint.h
////////////////////////////////////////////////////////////////////////////////

#include "Joint.h"

using namespace PK;

/**
 * Create an instance of a Joint
 *
 * @box1			Box 1 to join to...
 * @box2			... Box 2, which is this
 * @anchorPoint		Point to anchor them both to, part they can rotate around
 */
Joint::Joint(Shape* box1, Shape* box2, const Vector2f anchorPoint)
{
	Initialize(box1, box2, anchorPoint);
}

/**
 * Destroy this instance of a Joint
 */
Joint::~Joint()
{
}

/**
 * Reinitialize this instance of a Joint
 *
 * @box1			Box 1 to join to...
 * @box2			... Box 2, which is this
 * @anchorPoint		Point to anchor them both to, part they can rotate around
 */
void Joint::Initialize(Shape* box1, Shape* box2, const Vector2f anchorPoint)
{
	//Store the boxes to constrain
	m_box1 = box1;
	m_box2 = box2;

	//For the joint, what we want is to have two boxes, connected at their centers and with an arbitary point somewhere for us to rotate around 
	// (the anchor point) (Revolute Joint)
	//Here, we calculate the distance from the centers of the boxes to the anchor point
	m_box1Anchor = anchorPoint - m_box1->Position;
	m_box2Anchor = anchorPoint - m_box2->Position;

	//
	m_biasFactor = 0.2f;
}

/**
 * 
 */
void Joint::Update(float deltaTime)
{
	//Calculate the K matrix
	//I have written a bunch of stuff for the derivation of this matrix: https://docs.google.com/document/d/1cqSkJ61gNN3U71xgvXQyvhVOCRTr2NGl0brIjdDPceY/edit?usp=sharing

	//K = [1/m1+1/m2     0    ] + invI1 * [r1.y*r1.y -r1.x*r1.y] + invI2 * [r1.y*r1.y -r1.x*r1.y]
	//    [    0     1/m1+1/m2]           [-r1.x*r1.y r1.x*r1.x]           [-r1.x*r1.y r1.x*r1.x]

	//Calculate the rotation of the anchors (the distances from the point to the box)
	Matrix22 box1Rotation = Matrix22(m_box1->Rotation);
	Matrix22 box2Rotation = Matrix22(m_box2->Rotation);

	m_box1RotatedAnchor = box1Rotation * m_box1Anchor;
	m_box2RotatedAnchor = box2Rotation * m_box2Anchor;

	//Create the 3 parts of the K matrix
	Matrix22 kPart1 = Matrix22();
	Matrix22 kPart2 = Matrix22();
	Matrix22 kPart3 = Matrix22();

	//Calculate Matrix number 1
	kPart1.Column1.X = m_box1->InvMass + m_box2->InvMass;												kPart1.Column2.X = 0.0f;
	kPart1.Column1.Y = 0.0f;																			kPart1.Column2.Y = m_box1->InvMass + m_box2->InvMass;

	//Calculate Matrix number 2
	kPart2.Column1.X =  m_box1->InvInertiaTensor * m_box1RotatedAnchor.Y * m_box1RotatedAnchor.Y;		kPart2.Column2.X = -m_box1->InvInertiaTensor * m_box1RotatedAnchor.X * m_box1RotatedAnchor.Y;
	kPart2.Column1.Y = -m_box1->InvInertiaTensor * m_box1RotatedAnchor.X * m_box1RotatedAnchor.Y;		kPart2.Column2.Y =  m_box1->InvInertiaTensor * m_box1RotatedAnchor.X * m_box1RotatedAnchor.X;

	//Calculate Matrix number 3
	kPart3.Column1.X =  m_box2->InvInertiaTensor * m_box2RotatedAnchor.Y * m_box2RotatedAnchor.Y;		kPart3.Column2.X = -m_box2->InvInertiaTensor * m_box2RotatedAnchor.X * m_box2RotatedAnchor.Y;
	kPart3.Column1.Y = -m_box2->InvInertiaTensor * m_box2RotatedAnchor.X * m_box2RotatedAnchor.Y;		kPart3.Column2.Y =  m_box2->InvInertiaTensor * m_box2RotatedAnchor.X * m_box2RotatedAnchor.X;

	//Combine the 3 matrixes!
	Matrix22 K = kPart1 + kPart2 + kPart3;
	
	//Invert for the kP = V calculation. Check the bottom of the Google Doc for the reason why: https://docs.google.com/document/d/1cqSkJ61gNN3U71xgvXQyvhVOCRTr2NGl0brIjdDPceY/edit?usp=sharing
	m_invK = Matrix22(K);
	m_invK.Invert();

	//"The error is the separation between the anchor points" << from the Google doc, this should be 0 so that the constraint is being adhered to. Calculate the difference
	Vector2f p1 = m_box1->Position + m_box1RotatedAnchor;
	Vector2f p2 = m_box2->Position + m_box2RotatedAnchor;
	Vector2f dp = p2 - p1;

	//Calculate the bias from the value above
	m_bias = m_biasFactor * (1 / deltaTime) * dp;
}

/**
 * 
 */
void Joint::ApplyImpulse()
{
	//Calculate the tangents to both of the relative positions
	Vector2f box1RelativeTangent = Tangent(m_box1RotatedAnchor);
	Vector2f box2RelativeTangent = Tangent(m_box2RotatedAnchor);

	//Calculate the relative velocity at contact point for each of the objects
	//How is the AngularVelocity >> Linear Velocity  conversion done?
	// Thinking about angular velocity, if a box was rotating clockwise (+ive) and hits thro ground, the rotation would cause a linear
	// velocity to to the left. So we use the relative position of each of the boxes to the contact point, find the tangent to that and
	// multiply that by the angular velocity.
	//AngularTangent is negated here because the tangent calculated is in the opposite direction to what we need (we need to the right)
	Vector2f relativeVelocity = m_box2->Velocity + (box2RelativeTangent * -m_box2->AngularVelocity) - m_box1->Velocity - (box1RelativeTangent * -m_box1->AngularVelocity);

	//Calcualte the impulse. This is using the equation we were fretting about above, at the bottom of the google doc (P = ^V * k^-1)
	Vector2f impulse = m_invK * -1 * (m_bias + relativeVelocity);

	//Apply the impulse to the boxes. Angular velocity equation taken from this (as said by the BoxPair): http://en.wikipedia.org/wiki/Collision_response#Computing_Impulse-Based_Reaction
	m_box1->Velocity -= m_box1->InvMass * impulse;
	m_box1->AngularVelocity -= m_box1->InvInertiaTensor * Cross(m_box1RotatedAnchor, impulse);

	m_box2->Velocity += m_box2->InvMass * impulse;
	m_box2->AngularVelocity += m_box2->InvInertiaTensor * Cross(m_box2RotatedAnchor, impulse);
}

/**
 * Get the joint anchors for both of the boxes
 */
void Joint::GetJointAnchors(Vector2f& box1Anchor, Vector2f& box2Anchor)
{
	box1Anchor = m_box1Anchor;
	box2Anchor = m_box2Anchor;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: CollisionCircleCircle.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Collision.h"

/**
 * Calculate the collision data between two circles
 *
 * @circle1			Circle 1 to try and collide with...
 * @circle2			... Circle 2, which is this
 * @contacts		Contacts array for us to store our contacts in
 */
int PK::CircleCircleCollision(Circle* circle1, Circle* circle2, Contact* contacts)
{
	//Algorithm for detecting collision is taken rom th lecture notes, PP 2.3
	//Calculate the squared length of the distance between the circles
	Vector2f separatingDistance = circle2->Position - circle1->Position;
	float lengthSquared = Dot(separatingDistance, separatingDistance);

	//Calculate the squared addition of the two radii
	float radii = circle1->Radius + circle2->Radius;
	radii *= radii;

	//if the length is less than the length of the distance, then collision!
	if (lengthSquared < radii)
	{
		//Like I said, Collision!
		//Everything after this is completely made up.
		//I'm currently taking the contact point as the point 1/2 way between the two circle centers.
		//Sorry for the sqrt
		int numContacts = 1;
		float separation = radii - lengthSquared;
		separatingDistance.Normalize();

		contacts[0].Separation = (FastSqrt(separation) * -1) + 0.3f;
		contacts[0].Normal = separatingDistance;
		contacts[0].Position = circle1->Position + (separatingDistance * (FastSqrt(lengthSquared) * 0.5f));
		
		return 1;
	}
	
	//Else no collision!
	return 0;
}
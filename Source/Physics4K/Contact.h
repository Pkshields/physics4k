////////////////////////////////////////////////////////////////////////////////
// Filename: Contact.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Vector2f.h"

namespace PK
{
	class PK_API Contact
	{
	public:
		Contact();
		Contact(Vector2f pos, Vector2f nor, float sep);
		~Contact();

	public:
		Vector2f Position;			/**< Position of the contact */
		Vector2f Normal;			/**< Normal for the contact */
		float Separation;			/**< Separation of the contact from the incident face */

		float RIMNormal;			/**< Reaction Impulse Magnitude across the Normal of the contact point */
		float RIMTangent;			/**< Reaction Impulse Magnitude across the Tangent of the contact point */
		float PenetrationOffset;	/**< Additional velocity to apply to the impulse to sallow for the fact that the objects are inside each other, not like real world */
	};
}
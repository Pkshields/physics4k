////////////////////////////////////////////////////////////////////////////////
// Filename: Shape.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Shape.h"

using namespace PK;

/**
 * Create an instance of a Shape object
 */
Shape::Shape()
{
	Position.X = 0.f;
	Position.Y = 0.f;

	Friction = 0.2f;

	Rotation = 0.f;

	//If the mass is set to FLT_MAX, then we don't want the shape to move or be affected by any force. 
	//Set the values accordingly to allow that to happen.
	Mass = FLT_MAX;
	InvMass = 0.f;
	InertiaTensor = FLT_MAX;
	InvInertiaTensor = 0.f;

	Velocity = Vector2f(0.f, 0.f);
	AngularVelocity = 0.f;
	Force = Vector2f(0.f, 0.f);

	Type = ShapeType::NoneShape;
}

/**
 * Create an instance of a Shape object
 */
Shape::Shape(float xPos, float yPos, float rotation, float friction, float mass, float inertiaTensor)
{
	Position.X = xPos;
	Position.Y = yPos;

	Friction = friction;

	Rotation = rotation;

	if (mass != FLT_MAX)
	{
		Mass = mass;
		InvMass = 1.f / Mass;
		InertiaTensor = inertiaTensor;
		InvInertiaTensor = 1.f / InertiaTensor;
	}
	else
	{
		//If the mass is set to FLT_MAX, then we don't want the shape to move or be affected by any force. 
		//Set the values accordingly to allow that to happen.
		Mass = FLT_MAX;
		InvMass = 0.f;
		InertiaTensor = FLT_MAX;
		InvInertiaTensor = 0.f;
	}

	Velocity = Vector2f(0.f, 0.f);
	AngularVelocity = 0.f;
	Force = Vector2f(0.f, 0.f);
}

/**
 * Set a static external force to be applied to the shape next update
 *
 * @force	Force to be applied
 */
void Shape::SetForce(Vector2f force)
{
	Force.X = force.X;
	Force.Y = force.Y;
}

/**
 * Reset the external force to zero
 */
void Shape::ZeroForce()
{
	Force.X = 0.f;
	Force.Y = 0.f;
}

/**
 * Gets the currennt Type of shape this is
 */
ShapeType Shape::GetType()
{
	return Type;
}
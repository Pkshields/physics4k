////////////////////////////////////////////////////////////////////////////////
// Filename: Circle.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Circle.h"

using namespace PK;

/**
 * Create an instance of a Circle object
 *
 * @xPos		Position of the circle in the X axis
 * @yPos		Position of the circle in the Y axis
 * @radius		Radius of the circle
 * @rotation	Rotation of the circle in radians
 * @mass		Mass of the circle
 */
Circle::Circle(float xPos, float yPos, float radius, float rotation, float mass)
{
	Initialize(xPos, yPos, radius, rotation, mass);

	Type = ShapeType::CircleType;
}

/**
 * Create an instance of a Circle object
 *
 * @xPos		Position of the circle in the X axis
 * @yPos		Position of the circle in the Y axis
 * @radius		Radius of the circle
 * @rotation	Rotation of the circle in radians
 * @mass		Mass of the circle
 * @friction	Coefficient of friction for the circle
 */
Circle::Circle(float xPos, float yPos, float radius, float rotation, float mass, float friction)
{
	Initialize(xPos, yPos, radius, rotation, mass, friction);

	Type = ShapeType::CircleType;
}

/**
 * Destroy this instance of a circle object
 */
Circle::~Circle()
{
}

/**
 * Reinitialize this instance of a Circle object
 *
 * @xPos		Position of the circle in the X axis
 * @yPos		Position of the circle in the Y axis
 * @radius		Radius of the circle
 * @rotation	Rotation of the circle in radians
 * @mass		Mass of the circle
 */
void Circle::Initialize(float xPos, float yPos, float radius, float rotation, float mass, float friction)
{
	Position.X = xPos;
	Position.Y = yPos;

	Friction = friction;

	Radius = radius;
	Rotation = rotation;

	if (mass != FLT_MAX)
	{
		Mass = mass;
		InvMass = 1.f / Mass;
		InertiaTensor = mass * (Radius * Radius + Radius * Radius) / 10.0f;		//<<Why divided by 12? http://techhouse.brown.edu/~dmorris/projects/tutorials/inertia.tensor.summary.pdf
		InvInertiaTensor = 1.f / InertiaTensor;
	}
	else
	{
		//If the mass is set to FLT_MAX, then we don't want the circle to move or be affected by any force. 
		//Set the values accordingly to allow that to happen.
		Mass = FLT_MAX;
		InvMass = 0.f;
		InertiaTensor = FLT_MAX;
		InvInertiaTensor = 0.f;
	}

	Velocity = Vector2f(0.f, 0.f);
	AngularVelocity = 0.f;
	Force = Vector2f(0.f, 0.f);
}

/**
 * Get the width and height of the circle as a Vector2f
 */
float Circle::GetRadius()
{
	return Radius;
}
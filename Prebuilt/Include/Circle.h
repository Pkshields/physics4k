////////////////////////////////////////////////////////////////////////////////
// Filename: Circle.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Shape.h"

namespace PK
{
	class PK_API Circle : public Shape
	{
	public:
		Circle(float xPos, float yPos, float radius, float rotation, float mass);
		Circle(float xPos, float yPos, float radius, float rotation, float mass, float friction);
		~Circle();
		
		void Initialize(float xPos, float yPos, float radius, float rotation, float mass, float friction = 0.2f);

		float GetRadius();

	public:
		float Radius;			/**< Radius of the circle */
	};
}
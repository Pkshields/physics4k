////////////////////////////////////////////////////////////////////////////////
// Filename: Matrix22.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include <math.h>
#include "Vector2f.h"

namespace PK
{
	class PK_API Matrix22
	{
	public:
		Matrix22();
		Matrix22(float angle);
		Matrix22(float col11, float col12, float col21, float col22);
		Matrix22(Vector2f col1, Vector2f col2);
		Matrix22(const Matrix22& copy);
		~Matrix22();

		void Transpose();
		void Transpose(Matrix22& mat);
		bool Invert();

		Matrix22& operator=(const Matrix22 &param);
		Matrix22& operator+= (const Matrix22 &param);
		Matrix22& operator-= (const Matrix22 &param);
		Matrix22& operator*= (const Matrix22 &param);
		Matrix22& operator*= (const float &param);
		const Matrix22 operator+ (const Matrix22 &param) const ;
		const Matrix22 operator- (const Matrix22 &param)const ;
		const Matrix22 operator* (const Matrix22 &param)const ;
		const Matrix22 operator* (const float &param)const ;

		const Vector2f operator* (const Vector2f &param)const ;

	public:
		Vector2f Column1;
		Vector2f Column2;
	};
}
////////////////////////////////////////////////////////////////////////////////
// Filename: MathXtras.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "Vector2f.h"
#include "Matrix22.h"

const float TWO_PI = 6.283185307179586476925286766559f;

using namespace PK;

namespace PK
{
	Vector2f Abs(const Vector2f& value);
	Matrix22 Abs(const Matrix22& value);
	float Sign(float x);
	float Dot(const Vector2f& a, const Vector2f& b);
	Vector2f Tangent(const Vector2f& vector);
	float Cross(const Vector2f& v1, const Vector2f& v2);
	float Clamp(float value, const float min, const float max);
	unsigned int Factorial(unsigned int n);
	unsigned int TriangleNumber(unsigned int n);
	float FastInvSqrt(float n);
	float FastSqrt(float n);
}
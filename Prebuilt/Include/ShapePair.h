////////////////////////////////////////////////////////////////////////////////
// Filename: BoxPair.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "PKInclude.h"
#include "Box.h"
#include "Contact.h"
#include "Collision.h"

namespace PK
{
	class PK_API ShapePair
	{
		friend class World;

	public:
		ShapePair(Shape* box1, Shape* box2);
		~ShapePair();

		void Update(float deltaTime);
		void ApplyImpulse();

	private:
		void CalculateContactData(float deltaTime);

	private:
		Shape* m_box1;			/**< Shape 1 to store */
		Shape* m_box2;			/**< Shape 2 to store */

		float combinedFriction;	/**< Coefficient value fo the combined friction */

		Contact m_contacts[2];	/**< Place to store the collision contacts */
		int m_numContacts;		/**< Container number of above array */
	};
}